

const whereAmI = async (lat, lng) => {
    try {
        const url = `https://geocode.xyz/${lat},${lng}?geoit=json&auth=336794345973518239765x25542`;
        const response = await fetch(url);
        const data = await response.json();
        if (!response.ok) {
            console.log(`Fetching Error: ${data.statusText}`);
            return
        }
        if (!data.country || !data.city) {
            console.log('Please check the lattitude or longtitude');
            return
        }
        if (data.country) {
            console.log('Your country is: ' + data.country);
        }
        if (data.city) {
            console.log('Your city is: ' + data.city);
        }
      
    } catch (error) {
        console.log(error);
    }
};

whereAmI(52.508, 13.381);
whereAmI(19.037, 72.873);
whereAmI(-33.933, 18.474);
whereAmI(33.933, 18.474);
whereAmI(43.933, 18.474);

